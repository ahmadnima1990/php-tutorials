<?php
// Variable scope
$a = 1;
$b = 2;

function Summ()
{
    global $a, $b;

    $b = $a + $b;
} 

Summ();
echo $b;
?>

<br />
<br />

<?php
// Variables Variable

$Ahmad = 'Nima';
$Ali = 'Ahmad';
$Behnam = 'Ali';
$Yasser = 'Behnam';
$Nima = 'Yasser';

echo $Nima; // Yasser
echo $$Nima; // Behnam
echo $$$Nima; // Ali
echo $$$$Nima; // Ahmad
echo $$$$$Nima; //Amin
?>