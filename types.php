<?php
// BOOLEAN
$action = true;
if($action == true){
	echo "every thing is ok";
}
?>

<br />
<br />

<?php 
// NUMBER
$int_num = 987;
echo $int_num;

/*
some number methods

floor()

ceil()
*/
?>

<br />
<br />

<?php 
// FLOATING POINT NUMBERS
$float_num = 3.14;
echo $float_num;
?>

<br />
<br />

<?php 
// STRINGS
$str = "This is a simple string ";
echo $str;

$apple = 'APPLE ';
echo " I love $apple";

$strA = ' my email is ';
$strB = ' a@amail.com';
echo $strA.$strB;

/*
SOME METHODS OR STRINGS

STRLEN()

strtoupper()

substr()

str_replace()

*/
?>

<br />
<br />

<?php 
// NUMERIC STRINGS
$str = 1 + "10.5";
echo $str;
?>

<br />
<br />

<?php
// ARRAY
$array1 = array(
    "foo" => "bar",
    "bar" => "foo",
);

echo $array1["foo"];

$array2 = [
    "foo" => "bar",
    "bar" => "foo",
];



$array3 = array(
    1    => "a",
    "1"  => "b",
    1.5  => "c",
    true => "d",
);

;

// indexed array
$array4 = array(" ahmad", "nima", "kian");
echo $array4[0];

?>

<br />
<br />

<?php
// Iterables
$numbers = [1, 2, 3, 4, 5];
 foreach ($numbers as $value) {
        echo $value;
    } 
?>


<br />
<br />

<?php
// Objects
class myClass
{
	function myName(){
		echo " my name is ahmad";
	}
    function goodDay()
    {
        echo "I had a good day yesterday "; 
    }
}

$bar = new myClass;
$bar->goodDay();
$bar->myName();
?>

<br />
<br />

<?php
// type declaration
function sum($a, $b): int {
    return $a + $b;
}

// Note that a float will be returned.
var_dump(sum(1, 2));
?>

<br />
<br />

